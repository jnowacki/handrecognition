import os
import cv2
import numpy as np
import math


class Hand:
    def __init__(self, imageName, path):
        self.imageName = imageName
        self.fingers_lengths = []
        self.process_image(path)

    def process_image(self, path):

        src_image = cv2.imread(path + self.imageName)
        scaled_image = cv2.resize(src_image, None, fx=0.2, fy=0.2, interpolation=cv2.INTER_CUBIC)

        # convert to gray scale
        gray_image = cv2.cvtColor(scaled_image, cv2.COLOR_BGR2GRAY)

        # blur image
        blurred_image = cv2.GaussianBlur(gray_image, (3, 3), 0)

        # extract edges
        edge_image = cv2.Canny(blurred_image, 100, 200)

        kernel = np.ones((5, 5), np.uint8)

        # image dilatation
        dilatated_image = cv2.dilate(edge_image, kernel, iterations=1)

        h, w = dilatated_image.shape[:2]
        mask = np.zeros((h + 2, w + 2), np.uint8)

        # fills outer area with white
        cv2.floodFill(dilatated_image, mask, (0, 0), (100, 100, 100))

        # replacing colours
        dilatated_image[dilatated_image != 100] = 255
        dilatated_image[dilatated_image != 255] = 0

        # find contours
        im2, contours, hierarchy = cv2.findContours(dilatated_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        # finding biggest contour
        length = len(contours)
        maxArea = -1

        if length > 0:
            for i in range(length):  # find the biggest contour (according to area)
                temp = contours[i]
                area = cv2.contourArea(temp)
                if area > maxArea:
                    maxArea = area
                    ci = i

                res = contours[ci]

        # draw biggest contour on image
        cv2.drawContours(scaled_image, [res], 0, (0, 255, 0), 2)

        hull = cv2.convexHull(res)
        # draw convex hull contour on image
        cv2.drawContours(scaled_image, [hull], 0, (0, 0, 255), 0)

        hull2 = cv2.convexHull(res, returnPoints=False)

        # detect defects
        defects = cv2.convexityDefects(res, hull2)

        # calculate center of hand shape
        M = cv2.moments(res)
        cx = int(M['m10'] / M['m00'])
        cy = int(M['m01'] / M['m00'])

        sum = 0
        count = 0

        potential_fingers = []

        # find placement of fingers
        for i in range(defects.shape[0]):  # calculate the angle
            s, e, f, d = defects[i][0]
            start = tuple(res[s][0])
            end = tuple(res[e][0])
            far = tuple(res[f][0])
            a = math.sqrt((end[0] - start[0]) ** 2 + (end[1] - start[1]) ** 2)
            b = math.sqrt((far[0] - start[0]) ** 2 + (far[1] - start[1]) ** 2)
            c = math.sqrt((end[0] - far[0]) ** 2 + (end[1] - far[1]) ** 2)
            angle = math.acos((b ** 2 + c ** 2 - a ** 2) / (2 * b * c))  # cosine theorem

            if angle > math.pi / 9 and start[1] < h * 0.7:
                count += 1
                sum += math.sqrt((start[0] - cx) ** 2 + (start[1] - cy) ** 2)
                potential_fingers.append(start)

        cv2.circle(scaled_image, (cx, cy), 8, [211, 84, 0], -1)

        potential_fingers.sort(key=lambda x: x[0])

        # calculate length of fingers
        for finger in self.filter_fingers(potential_fingers):
            self.fingers_lengths.append(math.sqrt((finger[0] - cx) ** 2 + (finger[1] - cy) ** 2))
            cv2.circle(scaled_image, finger, 8, [211, 84, 0], -1)

    # compares hand to set of other hands and sorts them based on mean squared error from the most alike to the least
    def compare_to_set(self, hands_to_compare):

        comparison_result = []

        for hand in hands_to_compare:
            sum_of_errors = 0
            i = 0

            if len(hand.fingers_lengths) != len(self.fingers_lengths):
                comparison_result.append('ERROR')
                continue

            for index, finger in enumerate(self.fingers_lengths):
                sum_of_errors += math.pow(finger - hand.fingers_lengths[index], 2)
                i += 1

            comparison_result.append((hand.imageName, sum_of_errors / i))

        comparison_result.sort(key=lambda x: x[1])

        print(self.imageName)
        for result in comparison_result:
            print(result)

    # filters fingers based on distance between them
    def filter_fingers(self, fingers):
        new_fingers = []
        temp = fingers[0]
        for i in range(1, len(fingers)):
            if fingers[i][0] - temp[0] > 45:
                new_fingers.append(temp)
                temp = fingers[i]
        new_fingers.append(temp)
        return new_fingers[0:5]


images_dir = '\images'
test_dir = '\\test'

originalHandsImages = os.listdir(os.getcwd() + images_dir)
testHandsImages = os.listdir(os.getcwd() + test_dir)

originalHands = []

for image in originalHandsImages:
    originalHands.append(Hand(image, os.getcwd() + images_dir + '\\'))

testHands = []

for image in testHandsImages:
    testHands.append(Hand(image, os.getcwd() + test_dir + '\\'))

for testHand in testHands:
    testHand.compare_to_set(originalHands)
    print('')
